#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import time
from apps.payment import BaseHandler
from tornado.escape import json_decode
from apps.core.client import create_mysql_client
from apps.core.model import PrintOrder, Student
from apps.core.model import ParseRedis

class WebHookChargeHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))

        signature = self.request.headers['X-Pingplusplus-Signature'].decode('utf-8')
        trade_no = response['data']['object']['order_no']

        if response['type'] == 'charge.succeeded':
            self.set_status(status_code=200, reason=u'验证成功')

            with create_mysql_client() as session:
                nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
                PrintOrder.update_attrs(session, attrs=dict(order_state=1, paid_at=nowtime),filter_by=dict(trade_no=trade_no))
                ParseRedis.set_attr(trade_no, 1, expire=60*5)
                self.write(u'验证成功')

        else:
            self.set_status(status_code=500, reason=u'服务器错误/验证失败')

            self.write(u'服务器错误/验证失败')

class WebHookRefundHandler(BaseHandler):
    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))

        trade_no = response['data']['object']['order_no']
        charge_id = response['data']['object']['charge']

        if response['type'] == 'refund.succeeded':
            self.set_status(status_code=200, reason=u'验证成功')

            with create_mysql_client() as session:

                nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
                PrintOrder.update_attrs(session, attrs=dict(canceled_at=nowtime,order_state=4),
                                        filter_by=dict(trade_no=trade_no, charge_id=charge_id))

                self.write(u'验证成功')

        else :
            self.set_status(status_code=500, reason=u'服务器错误/验证失败')
            self.write(u'服务器错误/验证失败')

# /webhook/wxpaystatus
class WechatCallBackHandler(BaseHandler):

    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        id = self.is_authorized(token)

        if id:
            cls = Student

        trade_no = self.get_argument('trade_no')
        sign = ParseRedis.get_attr(trade_no)
        self.return_success(results={'status':sign})