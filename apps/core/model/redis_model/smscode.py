#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import re
import random
import string
from sdks.ccprest.rest import REST
from apps.core.client import create_redis_client


settings = dict(
    accountSid = '8a48b551506fd26f01507136100b05c4',
    accountToken = '482bd8a5646e48e59850ac371d70e33f',
    appId ='8a48b551506fd26f01507499940b0bc1',
    serverIP ='app.cloopen.com',
    serverPort = '8883',
    softVersion ='2013-12-26'
)

class SmsCode(object):
    __slots__ = ['redis_client']

    redis_client = create_redis_client()

    @classmethod
    def send_sms(cls, username, temp_id=50540, expire=5):
        phone = re.match(r'^1(3\d|4[5,7]|5[0-35-9]|8[02-36-9])([-]?(\d{4})){2}', username) #chinese大陆手机号码
        if phone is None:
            return {'msg':u'手机号格式错误, 请重新填写', 'results': None, 'error':-1}
        if cls.is_exists('smscode' + str(username)):
            return {'msg':'您需要等待' + str(cls.redis_client.ttl('smscode' + str(username))) + 's才能重获验证码', 'results':None, 'error':-1}
        cls.set_attr('smscode' + str(username), 1, 60)

        #短信验证成功
        rest = REST(settings.get('serverIP'), settings.get('serverPort'), settings.get('softVersion'))
        rest.setAccount(settings.get('accountSid'), settings.get('accountToken'))
        rest.setAppId(settings.get('appId'))

        code = string.join(random.sample([str(_) for _ in xrange(9)], 6), '')
        data = [code, expire]
        results = rest.sendTemplateSMS(username, data, temp_id)

        if results['statusCode'] == '000000':
            cls.set_attr(username, code, expire*60)
            return { 'results':{'smscode':code}, 'msg':'短信验证码发送成功', 'error':0}
        else:
            return {'msg':'短信验证码发送失败', 'results':None, 'error':-1}

    @classmethod
    def is_exists(cls, key):
        return cls.redis_client.exists(key)

    @classmethod
    def set_attr(cls, key, value, expire=60):
        cls.redis_client.setex(key, value, expire)

    @classmethod
    def get_attr(cls, key):
        if cls.is_exists(key):
            return cls.redis_client.get(key)
        else:
            return None

    @classmethod
    def get_tll(cls, key):
        if cls.is_exists(key):
            return cls.redis_client.ttl(key)
        else:
            return None

if __name__ == '__main__':
    def func(error=-1, results=None, msg=''):
        print error, results, msg

    def ret():
        return {'error':0, 'results':'fuck u', 'msg':'come on'}

    func(**ret())