#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from apps.core.client import create_redis_client

week = 60 * 60 * 24 * 7

class ParseRedis(object):
    __slots__ = ['redis_client']

    redis_client = create_redis_client()

    @classmethod
    def get_attr(cls, key):
        if cls.redis_client.exists(key):
            return cls.redis_client.get(key)
        else:
            return None

    @classmethod
    def set_attr(cls, key, value, expire=None):
        if expire is None:
            expire = 604800
        cls.redis_client.setex(key, value, expire)
        return cls.redis_client.ttl(key)

    @classmethod
    def del_attr(cls, key):
        if cls.redis_client.exists(key):
            cls.redis_client.delete(key)
            return True
        else:
            return False