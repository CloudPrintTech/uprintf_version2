#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from .mysql_model.academy import *
from .mysql_model.building import *
from .mysql_model.city import *
from .mysql_model.company import *
from .mysql_model.major import *
from .mysql_model.print_order import *
from .mysql_model.print_task import *
from .mysql_model.school import *
from .mysql_model.shop_manager import *
from .mysql_model.student import *
from .mysql_model.classes import *
from .mysql_model.shop import *

from .redis_model.smscode import SmsCode
from .redis_model.parseredis import ParseRedis