#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT, TEXT, TIMESTAMP
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR
from sqlalchemy.sql.expression import text, null
from .basemodel import Model

class PrintOrder(Model):
    __tablename__ = 'print_order'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    trade_no = Column(VARCHAR(255))
    total_pages = Column(INTEGER(unsigned=True))
    student_id = Column(INTEGER(unsigned=True), ForeignKey("student.id"), index=True, default=null())
    building_id = Column(INTEGER(unsigned=True), ForeignKey("building.id"), index=True, default=null())
    shop_id = Column(INTEGER(unsigned=True), ForeignKey("shop.id"), index=True, default=null())
    manager_id = Column(INTEGER(unsigned=True), ForeignKey("shopmanager.id"), index=True, default=null())
    ads_id = Column(INTEGER(unsigned=True), ForeignKey("advertise.id"), index=True, default=null())

    charge_id = Column(VARCHAR(255), unique=True, default=null()) #pingpp的支付,退款所要需要的charge对象的id
    file_url = Column(VARCHAR(100))

    total_fee = Column(INTEGER(unsigned=True))
    delivery_fee = Column(INTEGER(unsigned=True))
    student_room = Column(VARCHAR(255))
    student_phone = Column(VARCHAR(13))
    note = Column(TEXT)
    note_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    demand_time = Column(VARCHAR(255))
    created_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    paid_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    printed_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    canceled_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    delivered_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    comment_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    comment = Column(TEXT)
    complain = Column(TEXT)
    complain_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    rate = Column(TINYINT(unsigned=True), default=0)
    order_state = Column(TINYINT(unsigned=True), default=0)
    order_type = Column(TINYINT(unsigned=True), default=0)

Index("student_index",  PrintOrder.student_id, unique=True)
Index("building_index", PrintOrder.building_id,unique=True)
Index("shop_index",  PrintOrder.shop_id, unique=True)
Index("manager_index", PrintOrder.manager_id, unique=True)
Index("ads_index", PrintOrder.ads_id, unique=True)