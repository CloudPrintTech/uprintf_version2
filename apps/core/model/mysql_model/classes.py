#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from .basemodel import Model

class Class(Model):
    __tablename__ = 'class'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    major_id = Column(INTEGER(unsigned=True),ForeignKey('major.id'))
    name = Column(VARCHAR(255))
