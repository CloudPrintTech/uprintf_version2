#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import null
from .basemodel import Model

class School(Model):
    __tablename__ = 'school'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    name = Column(VARCHAR(255), index=True)
    nickname = Column(VARCHAR(255))
    city_id = Column(INTEGER(unsigned=True), ForeignKey("city.id"), index=True, default=null())

    maxlat = Column(FLOAT)
    minlat = Column(FLOAT)
    maxlng = Column(FLOAT)
    minlng = Column(FLOAT)
    attr = Column(VARCHAR(255))
    status = Column(TINYINT(unsigned=True))
    addtime = Column(INTEGER(unsigned=True))
    modtime = Column(INTEGER(unsigned=True))

Index("school_name&city_id", School.name, School.city_id, unique=True)
