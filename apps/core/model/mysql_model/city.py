#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from .basemodel import Model

class City(Model):
    __tablename__ = 'city'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    name = Column(VARCHAR(255), index=True)
    alpha = Column(VARCHAR(255))
    firstalpha = Column(VARCHAR(255))
    attr = Column(VARCHAR(255))
    status = Column(TINYINT(unsigned=True))
    addtime = Column(INTEGER(unsigned=True))
    modtime = Column(INTEGER(unsigned=True))


Index("city_name", City.name, unique=True)
