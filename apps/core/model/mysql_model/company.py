#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT, TEXT
from sqlalchemy.schema import Column
from sqlalchemy.types import  VARCHAR
from .basemodel import Model

class Company(Model):
    __tablename__ = 'company'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    username = Column(VARCHAR(13)) #一般username都是手机号, 为了统一, 作为登陆账号
    password = Column(VARCHAR(32))
    name = Column(VARCHAR(255))
    phone = Column(VARCHAR(13))
    ID_number = Column(VARCHAR(255))
    ID_card = Column(TEXT)
    company_name = Column(VARCHAR(255)) #公司名
    company_type = Column(VARCHAR(255)) #公司性质,类型
    company_address = Column(VARCHAR(255)) #公司地址
    legalman_name = Column(VARCHAR(255)) #法人代表名字
    business_license = Column(TEXT) #营业执照照片
    contract = Column(VARCHAR(255))
    register_ID = Column(VARCHAR(255))
    state = Column(TINYINT(unsigned=True))
    balance = Column(INTEGER(unsigned=True)) #充值余额

