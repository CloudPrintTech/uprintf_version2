#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT, TEXT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import null
from .basemodel import Model

class ShopManager(Model):
    __tablename__ = 'shop_manager'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    building_id = Column(INTEGER(unsigned=True), ForeignKey('building.id'), index=True, default=null())
    school_id = Column(INTEGER(unsigned=True), ForeignKey("school.id"), index=True, default=null())
    username = Column(VARCHAR(13), unique=True)
    password = Column(VARCHAR(32))
    name = Column(VARCHAR(255))
    qq = Column(VARCHAR(255))
    sex = Column(TINYINT(unsigned=True), default=0) #男0, 女1
    graduate_year = Column(VARCHAR(4))
    alipay_account = Column(VARCHAR(255))
    certificate = Column(VARCHAR(255))
    ID_card = Column(TEXT)
    dorm = Column(VARCHAR(100))
    enroll = Column(VARCHAR(4))
    email = Column(VARCHAR(255))
    state = Column(TINYINT(unsigned=True), default=0)
    ID_number = Column(VARCHAR(255), unique=True)

Index("building_index", ShopManager.building_id, unique=True)
Index("school_index", ShopManager.school_id, unique=True)
