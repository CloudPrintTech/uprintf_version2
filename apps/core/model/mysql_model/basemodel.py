#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.ext.declarative import declarative_base
from functools import wraps
from inspect import isgenerator
import datetime

BaseModel = declarative_base()

def standard_time(func):

    def operation(query):
        if hasattr(query, 'note_at'):
            if getattr(query, 'note_at') :
                query.note_at = query.note_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'created_at'):
            if getattr(query, 'created_at'):
                query.created_at = query.created_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'paid_at'):
            if getattr(query, 'paid_at'):
                query.paid_at = query.paid_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'printed_at'):
            if getattr(query, 'printed_at'):
                query.printed_at = query.printed_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'canceled_at'):
            if getattr(query, 'canceled_at'):
                query.canceled_at = query.canceled_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'delivered_at'):
            if getattr(query, 'delivered_at'):
                query.delivered_at = query.delivered_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'comment_at'):
            if getattr(query, 'comment_at'):
                query.comment_at = query.comment_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'complain_at'):
            if getattr(query, 'complain_at'):
                query.complain_at = query.complain_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'uploaded_at'):
            if getattr(query, 'uploaded_at'):
                query.uploaded_at = query.uploaded_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'updated_at'):
            if getattr(query, 'updated_at'):
                query.updated_at = query.updated_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'deleted_at'):
            if getattr(query, 'deleted_at'):
                query.deleted_at = query.deleted_at.strftime("%Y-%m-%d %H:%M:%S")
        if hasattr(query, 'downloaded_at'):
            if getattr(query, 'downloaded_at'):
                query.downloaded_at = query.downloaded_at.strftime("%Y-%m-%d %H:%M:%S")

    @wraps
    def wrappers(cls, *args, **kwargs):
        query = func(cls, *args, **kwargs)
        if isinstance(query, datetime.datetime):
            query =  query.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(query, (tuple, list)):
            for item in query:
                operation(item)
        else:
            operation(query)
        return query
    return wrappers


class Model(BaseModel):
    __abstract__ = True
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }

    @classmethod
    def get_by_id(cls, session, id, columns=None, lock_mode=None):
        if hasattr(cls, id):
            scalar = False
            if columns:
                if isinstance(columns, (tuple, list)):
                    query = session.query(*columns)
                else:
                    query = session(columns)
                    scalar = True
            else:
                query = session.query(cls)
            if lock_mode:
                query = query.lock_mode(lock_mode)
            query = query.filter(cls.id == id)
            if scalar:
                return query.scalar()
            else:
                return query.first() # 使用one()会报NoneType错误,还是不用的好

    @classmethod
    def update_attrs(cls, session, id=None, attrs=None, synchronize_session=False, filter_by=None):
        if hasattr(cls, 'id'):
            session.query(cls).filter(cls.id == id).update(
                values=attrs, synchronize_session=synchronize_session
            )
            session.commit()
        else:
            if filter_by:
                session.query(cls).filter_by(**filter_by).update(
                     values=attrs, synchronize_session=synchronize_session
                )

    @classmethod
    def get_all(cls, session, columns=None, offset=None, limit=None,
                order_by=None, not_null=None, lock_mode=None, filter_by=None): #reversed=False表示正序
        if columns:
            if isinstance(columns, (list, tuple)):
                query = session.query(*columns)
            else:
                query = session.query(columns)
        else:
            query = session.query(cls)
        if order_by:
            if isinstance(order_by, (list, tuple)):
                query = query.order_by(*order_by)
            else:
                query = query.order_by(order_by)
        if offset:
            query = query.offset(offset)
        if limit:
            query = query.limit(limit)
        if filter_by:
            if isinstance(filter_by, dict):
                query = query.filter_by(**filter_by)
        if not_null:
            if isinstance(not_null, (list, tuple)):
                for item in not_null:
                    query = query.filter(item!=None)
            else :
                query = query.filter(not_null!=None)
        if lock_mode:
            query = query.with_lockmode(lock_mode)
        return query.all()

    @classmethod
    def get_one(cls, session, columns=None, lock_mode=None, filter_by=None):
        if columns:
            if isinstance(columns, (list, tuple)):
                query = session.query(*columns)
            else:
                query = session.query(columns)
        else:
            query = session.query(cls)
        if filter_by:
            if isinstance(filter_by, dict):
                query = query.filter_by(**filter_by)
        if lock_mode:
            query = query.with_lockmode(lock_mode)
        return query.first()

    @classmethod
    def add_attrs(cls, session, attrs):
        if isinstance(attrs, dict):
            instance = cls(**attrs)
            session.add(instance)
            session.commit()
            return instance
