#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import null
from .basemodel import Model

class Building(Model):
    __tablename__ = 'building'

    id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True)
    school_id = Column(INTEGER(unsigned=True), ForeignKey("school.id"), index=True, default=null())
    deliver_id = Column(INTEGER(unsigned=True), ForeignKey("manager.id"), index=True, default=null()) #对应于楼长manager_id
    name = Column(VARCHAR(255))
    nickname = Column(VARCHAR(255))
    maxlat = Column(FLOAT, default=0.0)
    maxlng = Column(FLOAT, default=0.0)
    minlat = Column(FLOAT, default=0.0)
    minlng = Column(FLOAT, default=0.0)
    status = Column(TINYINT, default=0)
    attr = Column(VARCHAR(255))
    addtime = Column(INTEGER(unsigned=True))
    modtime = Column(INTEGER(unsigned=True))
    indexs = Column(TINYINT(unsigned=True), default=0) #用于排序获取name, 从小到大



Index("school_index", Building.school_id,  unique=True)
Index("manager_index", Building.deliver_id, unique=True)

