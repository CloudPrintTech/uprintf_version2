#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from .basemodel import Model

class Major(Model):
    __tablename__ = 'major'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    academy_id = Column(INTEGER(unsigned=True), ForeignKey("academy.id"), index=True)
    name = Column(VARCHAR(255))