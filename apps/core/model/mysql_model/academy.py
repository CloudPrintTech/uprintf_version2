#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from .basemodel import Model

class Academy(Model):
    __tablename__ = 'academy'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    school_id = Column(INTEGER(unsigned=True), ForeignKey("school.id"), index=True)
    enroll = Column(VARCHAR(4))
    name = Column(VARCHAR(255))
