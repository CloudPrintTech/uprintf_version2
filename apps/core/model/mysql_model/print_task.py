#-*- coding:utf-8 -*-
__author__ = 'raytlty'


from sqlalchemy.dialects.mysql import INTEGER, TINYINT, TEXT, TIMESTAMP
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import text, null
from .basemodel import Model

class PrintTask(Model):
    __tablename__ = 'print_task'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    order_id = Column(INTEGER(unsigned=True), ForeignKey("print_order.id"), index=True, default=null())
    student_id = Column(INTEGER(unsigned=True), ForeignKey("student.id"), index=True, default=null())
    file_name = Column(VARCHAR(255))
    pages = Column(INTEGER(unsigned=True))
    file_url = Column(VARCHAR(100))
    file_key = Column(VARCHAR(32), unique=True)
    bothside = Column(TINYINT(unsigned=True), default=0)
    colorful = Column(TINYINT(unsigned=True), default=0)
    copies = Column(TINYINT(unsigned=True), default=1)
    handouts = Column(TINYINT(unsigned=True), default=1)

    print_fee = Column(INTEGER(unsigned=True))
    print_pages = Column(INTEGER(unsigned=True))
    uploaded_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    updated_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    deleted_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    downloaded_at = Column(TIMESTAMP, default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    task_state = Column(TINYINT(unsigned=True), default=0)

Index("order_index", PrintTask.order_id, unique=True)
Index("student_index", PrintTask.student_id, unique=True)
