#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT, TEXT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import null
from .basemodel import Model

class Shop(Model):
    __tablename__ = 'shop'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    school_id = Column(INTEGER(unsigned=True), ForeignKey("school.id"), index=True, default=null())
    name = Column(VARCHAR(50))
    username = Column(VARCHAR(13), unique=True)
    password = Column(VARCHAR(32))
    localtion = Column(VARCHAR(255))
    legalman_name = Column(VARCHAR(255)) #法人代表名字
    alipay_account = Column(VARCHAR(255))
    tel = Column(VARCHAR(25))
    business_license = Column(TEXT)
    ID_card = Column(TEXT)
    register_ID = Column(VARCHAR(255))
    black1_price = Column(INTEGER(unsigned=True), default=9)
    black2_price = Column(INTEGER(unsigned=True), default=14)
    colorful1_price = Column(INTEGER(unsigned=True), default=90)
    colorful2_price = Column(INTEGER(unsigned=True), default=140)
    state = Column(TINYINT(unsigned=True), default=0)

Index("school_index", Shop.school_id, unique=True)
