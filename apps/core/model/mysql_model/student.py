#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.schema import Column, Index, ForeignKey
from sqlalchemy.types import  VARCHAR, FLOAT
from sqlalchemy.sql.expression import null
from .basemodel import Model

class Student(Model):
    __tablename__ = 'student'

    id = Column(INTEGER(unsigned=True), autoincrement=True, primary_key=True)
    building_id = Column(INTEGER(unsigned=True), ForeignKey('building.id'), index=True, default=null())
    school_id = Column(INTEGER(unsigned=True),ForeignKey('school.id'), index=True, default=null())
    academy_id = Column(INTEGER(unsigned=True),ForeignKey('academy.id'), index=True, default=null())
    major_id = Column(INTEGER(unsigned=True),ForeignKey('major.id'), index=True, default=null())
    class_id = Column(INTEGER(unsigned=True),ForeignKey('class.id'), index=True, default=null())
    openid = Column(VARCHAR(255), unique=True, default=null())
    name = Column(VARCHAR(255))
    username = Column(VARCHAR(13), unique=True) #username 统一是手机号用于登陆注册
    password = Column(VARCHAR(32))
    dorm = Column(VARCHAR(100))
    enroll = Column(VARCHAR(4))


Index("building_index", Student.building_id, unique=True)
Index("school_index", Student.school_id, unique=True)
Index("academy_index", Student.academy_id, unique=True)
Index("major_index", Student.major_id, unique=True)
Index("class_index", Student.class_id, unique=True)