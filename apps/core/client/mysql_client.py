#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from contextlib import contextmanager
import logging
import time

from sqlalchemy.engine import Connection, create_engine
from sqlalchemy.exc import IntegrityError, OperationalError
from sqlalchemy.orm import sessionmaker

from config import MYSQL_URL, DEBUG_MODE


LOCAL_MYSQL_URL = 'mysql+pymysql://root:root@localhost/cloudprinter_v2?charset=utf8'

mysql_engine =create_engine(LOCAL_MYSQL_URL, pool_size=150, pool_timeout=10, pool_recycle=3600,
                            echo=DEBUG_MODE, encoding='utf8')

Mysql_Session = sessionmaker(bind=mysql_engine)

@contextmanager
def create_mysql_client(expire_on_commit=False):
    session = Mysql_Session(expire_on_commit=expire_on_commit)
    try:
        yield session
    finally:
        session.close()

