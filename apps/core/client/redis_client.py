#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import redis
from config import REDIS_URL

DEFAULT_REDIS_ADDR = REDIS_URL


def create_redis_client():
    return redis.Redis(DEFAULT_REDIS_ADDR)

redis_client = create_redis_client()

def redis_has_exists(name):
    return redis_client.exists(name)

def redis_get_value(name):
    if redis_has_exists(name):
        return redis_client.get(name)
    else :
        return None

def redis_get_ttls(name):
    if redis_has_exists(name):
        return redis_client.ttl(name)
    else :
        return None

def redis_set_value(name, value, expire):
    redis_client.setex(name, value, 604800)