#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from apps.account.views import BaseHandler
from apps.core.client import create_mysql_client
from apps.core.model import Student, ShopManager
from apps.core.model import SmsCode
from tornado.escape import json_decode

# account/smscode

class SMSCodeHandler(BaseHandler):

    def post(self):

        response = json_decode(self.request.body.decode('utf-8'))

        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            username = response.get('username')
            with create_mysql_client() as session:
                query = cls.get_one(session, filter_by=dict(username=username))
                if query:
                    self.return_error(msg=u'请直接登录')

            if username:
                ret = SmsCode.send_sms(username)
                self.return_success(**ret)
            else:
                reason = u'请填写手机号码'
                self.set_status(400, reason)
                self.return_error(msg=reason)





if __name__ == '__main__':
    print 'fuck'