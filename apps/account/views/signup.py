#-*- coding:utf-8 -*-
__author__ = 'raytlty'

# account/signup
import hashlib
from config import ENCRYPT_PASSWORD, ENCRYPT_TOKEN
from apps.core.model import Student, ShopManager
from apps.core.model import SmsCode
from apps.core.client import create_mysql_client
from apps.account.views import BaseHandler
from tornado.escape import json_decode

class SignupHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            username = response.get('username')
            with create_mysql_client() as session:
                query = cls.get_one(session, filter_by=dict(username=username))
                if query:
                    self.return_error(msg=u'该手机号已经注册,请登录')

                if not SmsCode.is_exists(username):
                    self.return_error(msg=u'验证码已过期，请重新请求发送')

                smscode = response.get('smscode')
                print smscode
                if SmsCode.get_attr(username) != smscode.encode('utf-8'):
                    self.return_error(msg=u"输入的验证码错误")

                password = response.get('password')
                new_passwd = hashlib.md5((ENCRYPT_PASSWORD % (password, username)).encode()).hexdigest()

                cls.add_attrs(session, attrs=dict(username=username, password=new_passwd))

                token = hashlib.md5((ENCRYPT_TOKEN % (username, smscode)).encode()).hexdigest()
                self.return_success(results={'token':token, 'status':0})


# account/forget
class ForgetPasswordHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')
        id = self.is_authorized(token)

        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            with create_mysql_client() as session:
                username = response.get('username')

                if not SmsCode.is_exists(username):
                    self.return_error(msg=u'验证码已过期，请重新请求发送')

                smscode = response.get('smscode')
                if SmsCode.get_attr(username) != smscode.encode('utf-8'):
                    self.return_error(msg=u"输入的验证码错误")

                new_password = response.get('new_password')
                password = hashlib.md5((ENCRYPT_PASSWORD % (new_password, username)).encode()).hexdigest()

                cls.update_attrs(session, attrs=dict(password=password))

                self.return_success(msg=u'请重新登陆')

