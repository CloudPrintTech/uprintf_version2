#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from collections import defaultdict
from apps.core.model import Student, ShopManager, School, Building, Major
from apps.core.client import create_mysql_client
from apps.account.views import BaseHandler
from tornado.escape import json_decode

# account/info
class InfoHandler(BaseHandler):

    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        id = self.is_authorized(token)

        types = self.get_argument('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            with create_mysql_client() as session:
                results = defaultdict(dict)
                query = cls.get_by_id(session, id=id)

                func = lambda model, id: model.get_by_id(session, columns=model.name, id=id)
                for key, value in {'school':(School, query.school_id),
                                   'building':(Building, query.building_id)}.iteritems():
                    results[key] = {
                        'id':value[1],
                        'name':func(value[0], value[1])
                    }
                results['dorm'] = query.dorm
                results['enroll'] = query.enroll
                results['name'] = query.name
                if cls is Student:
                    results['major'] = Major.get_by_id(session, id=query.major_id, columns=Major.name)

                self.return_success(results=results, msg=u'获取信息成功')



    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')

        id = self.is_authorized(token)

        print id
        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            with create_mysql_client() as session:
                school = response.get('school')
                building = response.get('building')
                dorm = response.get('dorm')
                enroll = response.get('enroll')
                name = response.get('name')
                major = response.get('major')

                cls.update_attrs(session, attrs=dict(school_id=school.get('id'), building_id=building.get('id'),
                                                     dorm=dorm, enroll=enroll, name=name))

                if cls is Student:
                    cls.update_attrs(session, attrs=dict(major_id=Major.get_one(session, Major.id, filter_by=dict(name=major))))

                self.return_success(results={'status':0}, msg=u'修改信息成功')


if __name__ == '__main__':
    import sys
    print sys.path