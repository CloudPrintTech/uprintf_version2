#-*- coding:utf-8 -*-
__author__ = 'raytlty'


import hashlib
from collections import defaultdict
from config import ENCRYPT_PASSWORD, ENCRYPT_TOKEN
from apps.core.model import Student, ShopManager, PrintTask, Building, Shop
from apps.core.model import ParseRedis
from apps.core.client import create_mysql_client
from apps.account.views import BaseHandler
from tornado.escape import json_decode

# account/login

class LoginHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        username = response.get('username').encode()

        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()
        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            with create_mysql_client() as session:
                query = cls.get_one(session, filter_by=dict(username=username))
                print query
                password = response.get('password')
                if query is None:
                    self.return_error(msg=u'请先用手机号注册')

                else:
                    state = 0
                    openid = ""
                    if hashlib.md5((ENCRYPT_PASSWORD % (password, username)).encode()).hexdigest() != query.password:
                        self.return_error(msg=u'密码不正确')
                    if isinstance(cls, ShopManager):
                        if query.state == 3:
                            self.return_error(msg=u'你已经辞职,无法登陆')
                        state = query.state
                    if isinstance(cls, Student):
                        openid = query.openid if query.openid else ""
                    token = hashlib.md5((ENCRYPT_TOKEN % (username, query.password)).encode()).hexdigest()
                    ttl_time = ParseRedis.set_attr(token, query.id)
                    print ttl_time
                    self.return_success(results={'token':token, 'openid':openid, 'state':state})


# account/checktoken
class CheckTokenHandler(BaseHandler):
    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        id = self.is_authorized(token)

        with create_mysql_client() as session:
            results = defaultdict(dict)
            columns = (Student.username, Student.nickname, Student.building_id, Student.school_id, Student.openid)
            results['student_info'] = dict(zip(('username', 'nickname', 'building_id',
                                                'school_id', 'openid'),
                                               Student.get_by_id(session, id, columns=columns)))
            building_id = results['student_info'].pop('building_id')
            name = Building.get_by_id(session, building_id, columns=Building.name)
            results['student_info']['building'] = dict(zip(('id', 'name'), (building_id, name)))

            func = lambda pattern, elements: dict(zip(pattern, elements))
            task_pattern = ('task_id', 'file_name', 'pages')

            results['todo_task'] = {
                'copy':[ func(task_pattern, item) for item  \
                          in PrintTask.get_all(session,columns=(PrintTask.id, PrintTask.file_name, PrintTask.pages),
                                               filter_by=dict(student_id=id, task_state=0, file_key=None)) ],
                'print' :[ func(task_pattern, item) for item  \
                          in PrintTask.get_all(session,columns=(PrintTask.id, PrintTask.file_name, PrintTask.pages),
                                               filter_by=dict(student_id=id, task_state=0), not_null=PrintTask.file_key)]
            }

            query = Shop.get_one(session, filter_by=dict(school_id=results['student_info']['school_id']))
            if query:
                results['price_rule'] = {
                    'one_face': {
                        'black': query.black1_price / 100.0,
                        'colorful': query.colorful1_price / 100.0
                    },
                    'two_face': {
                        'black': query.black2_price / 100.0,
                        'colorful': query.colorful2_price / 100.0
                    }
                }
                results['shop_id'] = query.id

            results['demand_time']=["8点-9点","11点-12点","14点-15点","17点-18 点","21点-22点"]
            self.return_success(results=results)

# account/password
class ChangePasswdHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')
        id = self.is_authorized(token)

        types = response.get('type')
        if types == -1:
            reason = u'错误字段'
            self.set_status(400, reason)
            self.return_error()

        cls = Student if types == 0 else ShopManager if types == 1 else None
        if cls is None:
            reason = u'使用了扩展字段'
            self.set_status(400, reason)
            self.return_error()
        else:
            with create_mysql_client() as session:
                query = cls.get_by_id(session, id)
                if query is None:
                    self.return_error(msg=u'请重新登录')

                old_password = response.get('old_password')
                if hashlib.md5((ENCRYPT_PASSWORD % (query.username, old_password)).encode()).hexdigest() != old_password:
                    self.return_error(msg=u'原密码错误,请重新输入')

                new_password = response.get('new_password')
                password = hashlib.md5((ENCRYPT_PASSWORD % (query.username, new_password)).encode()).hexdigest()

                cls.update_attrs(session, id, attrs=dict(password=password))

                token = hashlib.md5((ENCRYPT_TOKEN % (query.username, password)).encode()).hexdigest()
                ParseRedis.set_attr(token, id)

                self.return_success(msg=u'修改密码成功')

# account/logout
class LogoutHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')

        id = self.is_authorized(token)

        if id:
            ParseRedis.del_attr(token)

        self.return_success(msg=u'登出成功')

