#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from views.info import InfoHandler
from views.login import LoginHandler, LogoutHandler, ChangePasswdHandler, CheckTokenHandler
from views.signup import SignupHandler, ForgetPasswordHandler
from views.sms import SMSCodeHandler

handlers = [
    (r'/v2/account/signup', SignupHandler),
    (r'/v2/account/checktoken', CheckTokenHandler),
    (r'/v2/account/info', InfoHandler),
    (r'/v2/account/login', LoginHandler),
    (r'/v2/account/logout', LogoutHandler),
    (r'/v2/account/smscode', SMSCodeHandler),
    (r'/v2/account/password', ChangePasswdHandler),
    (r'/v2/account/forget', ForgetPasswordHandler),
]

