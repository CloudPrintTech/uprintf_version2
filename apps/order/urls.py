#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from .views.list import ListHandler
from .views.purchase import PurchaseHandler
from .views.submit import SubmitHadnler

handlers = [
    (r'/v2/order/list', ListHandler),
    (r'/v2/order/purchase', PurchaseHandler),
    (r'/v2/order/submit', SubmitHadnler)
]