#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from apps.core.client import create_mysql_client
from apps.core.model import Student, PrintOrder
from apps.account.views import BaseHandler

class ListHandler(BaseHandler):

    def get(self, *args, **kwargs):
        token = self.get_argument('token')
        id = self.is_authorized(token)

        types = self.get_argument('type')

        if types == 0:
            cls = Student
        else:
            reason = u'只有学生才能查询订单列表'
            self.set_status(401, reason)
            self.return_error(msg=u'验证身份失败')
            self.finish()

        with create_mysql_client() as session:
            orders = PrintOrder.get_all(session, columns=cls.id,filter_by=dict(student_id=id))
            results = list()
            for order in orders:
                results.append({'id':order.id, 'total_fee':order.total_fee})
            self.return_success(results=results, msg='获取列表成功')



