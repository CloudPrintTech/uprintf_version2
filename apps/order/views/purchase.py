#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from apps.core.client import create_mysql_client
from apps.core.model import Student, PrintOrder
from apps.core.model import ParseRedis
from apps.account.views import BaseHandler
from tornado.escape import json_decode
from sdks.pingpays import PingppSDK
from cStringIO import StringIO
import qrcode
import base64


class PurchaseHandler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')
        id = self.is_authorized(token)

        client_ip = self.request.remote_ip

        types = response.get('type')

        if types == 0:
            cls = Student
        else:
            reason = u'只有学生才能下单'
            self.set_status(401, reason)
            self.return_error(msg=u'验证身份失败')
            self.finish()

        with create_mysql_client() as session:
            order_id = response.get('order_id')
            print_order = PrintOrder.get_by_id(session, id=order_id)
            if print_order is None:
                self.return_error(msg=u'请先确认下单')

            ParseRedis.set_attr(print_order.trade_no, 0, expire=60*5)  #5分钟

            charge, sign = PingppSDK.purchase(client_ip=client_ip, amount=print_order.total_fee, order_no=print_order.trade_no,
                                              channel=response.get('channel'), extra=response.get('extra'))
            if sign == False:
                self.return_error()

            PrintOrder.update_attrs(session, id=order_id, attrs=dict(order_state=0, charge_id=charge.get('id')))

            if response.get('channel') == 'wx_pub_qr':
                url = charge['credential']['wx_pub_qr']
                qr = qrcode.QRCode(
                    version=2,
                    error_correction=qrcode.constants.ERROR_CORRECT_H,
                    box_size=3,
                    border=3,
                )
                qr.add_data(url)
                img_io = StringIO()
                qr.make_image().save(img_io, 'PNG')
                img_io.seek(0)
                self.return_success(results={'charge':charge, 'qrcode':base64.b64encode(img_io.getvalue())}, msg=u'支付成功')

            self.return_success(results={'charge':charge}, msg=u'支付成功')


