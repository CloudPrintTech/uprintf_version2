#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import sys
reload(sys)
sys.path.append('/Volumes/WorkPlace/uprintf_version2/')

import tornado.web
import tornado.gen
from apps.core.model import ParseRedis

class BaseHandler(tornado.web.RequestHandler):
    def is_authorized(self, token):
        id = ParseRedis.get_attr(token)
        if id :
            return id
        else:
            reason = u'验证失效,请重新登陆'
            self.set_status(401, reason)
            self.finish(reason)

    def return_success(self, error=0, results=None, msg=u'返回成功'):
        self.write(dict(
            error = error,
            results = results if results else [],
            msg = msg,
        ))

    def return_error(self, error=-1, results=None, msg=u'Opps, 发生错误'):
        self.write(dict(
            error = error,
            results = results if results else [],
            msg = msg
        ))

class NotFoundHandler(BaseHandler):
    def get(self, *args, **kwargs):
        raise tornado.web.HTTPError(404)

    def post(self, *args, **kwargs):
        raise tornado.web.HTTPError(404)
