#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import time
import random
from tornado.escape import json_decode
from apps.core.client import create_mysql_client
from apps.core.model import Student, PrintOrder, Building, PrintTask
from apps.account.views import BaseHandler

settings = dict(
    BLACK_ONE_PRICE = 9,
    BLACK_TWO_PRICE = 14,
    COLOR_ONE_PRICE = 90,
    COLOR_TWO_PRICE = 140
)

# order/submit
class SubmitHadnler(BaseHandler):

    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')
        id = self.is_authorized(token) #学生id

        types = response.get('type')

        if types == 0:
            cls = Student
        else:
            reason = u'只有学生才能下单'
            self.set_status(401, reason)
            self.return_error(msg=u'验证身份失败')
            self.finish()

        with create_mysql_client() as session:
            unpay_order = PrintOrder.get_one(session, filter_by=dict(student_id=id, order_state=0))
            if unpay_order:
                self.return_error(msg=u'请先完成未完成订单')

            note = response.get('note')
            demand_time = response.get('demand_time')
            order_type = response.get('order_type')
            student_info = Student.get_by_id(session, id=id)
            building_info = Building.get_by_id(session, id=student_info.building_id)

            nowtime = time.strftime("%Y-%m-%d %H:%M:%S")
            trade_no = (str(int(time.time())) + str(random.randint(1000,10000)))[:15]

            total_fee = reduce(lambda x, y: x + y,
                            map(lambda task: task.get('copies') * ((task.get('pages') + \
                                task.get('handouts') * 2 - 1) / (task.get('handouts') * 2) *  (settings.get('COLOR_TWO_PRICE') if task.get('colorful') else settings.get('BLACK_TWO_PRICE')) \
                                       if task.get('bothside') else (task.get('pages') + task.get('handouts') - 1) / task.get('handouts') \
                                         * (settings.get('COLOR_ONE_PRICE') if task.get('colorful') else settings.get('BLACK_ONE_PRICE')))))

            total_pages = reduce(lambda x, y: x + y,
                            map(lambda task: (task.get('pages') + \
                                task.get('handouts') * 2 - 1) / (task.get('handouts') * 2) \
                                       if task.get('bothside') else (task.get('pages') + task.get('handouts') - 1) / task.get('handouts')))

            print_order = PrintOrder.add_attrs(session, attrs=dict(
                student_id=id, total_pages=total_pages, building_id=building_info.id,
                manager_id=building_info.deliver_id, total_fee=total_fee,student_room=student_info.dorm,
                student_phone=student_info.username, note=note, note_at=nowtime, created_at=nowtime,
                demand_time=demand_time, order_type=order_type, trade_no=trade_no
            ))

            map(lambda task, PrintTask=PrintTask, order_id=print_order.id: \
                PrintTask.update_attrs(session, id=task.get('id'), attrs=dict(
                    order_id=order_id, task_state=1)), tasks)

            self.return_success(results={'id':print_order.id, 'total_fee':total_fee}, msg=u'下单成功')





if __name__ == '__main__':
    tasks = [{'id':1, 'pages':5, 'handouts':2, 'copies':3, 'colorful':0}, {'id':2, 'pages':1, 'handouts':2, 'copies':3, 'colorful':1},
             {'id':3, 'pages':2, 'handouts':2, 'copies':3}, {'id':1, 'pages':4, 'handouts':2, 'copies':3}
             ,{'id':4, 'pages':19, 'handouts':2, 'copies':3}, {'id':5, 'pages':6, 'handouts':2, 'copies':3},
             {'id':6, 'pages':20, 'handouts':2, 'copies':3}, {'id':7,'pages':15, 'handouts':2, 'copies':3}]
    total_fee = 0
    func = lambda task, print_fee=0: print_fee + task.get('pages')

    a = ['a', 'b', 'c']
    b = ['d', 'e', 'f']
    print map(lambda aa, bb: (aa , bb), a, b)