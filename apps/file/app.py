#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import urls
import settings

import socket
import logging

import tornado.httpclient
import tornado.ioloop
import tornado.netutil
import tornado.web
import tornado.httpserver


logging.basicConfig(level=logging.INFO)

def get_application():
    return tornado.web.Application(
        handlers=urls.handlers,
        debug = settings.DEBUG,
        xsrf_cookies = settings.XSRF_COOKIES,
        gzip = settings.GZIP,
    )

def main():
    application = get_application()
    port = settings.PORT

    if settings.IPV4_ONLY:
        family = socket.AF_INET
    else:
        family = socket.AF_UNSPEC

    http_server = tornado.httpserver.HTTPServer(application, xheaders=settings.XHEADERS)

    sockets = tornado.netutil.bind_sockets(port, 'localhost', family=family) #显示IPV4的地址

    http_server.add_sockets(sockets)
    tornado.ioloop.IOLoop.instance().start()

def test():
    print settings.PORT
    print settings.MYSQL_URL


if __name__ == '__main__':
    main()