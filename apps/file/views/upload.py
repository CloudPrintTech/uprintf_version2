#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import os
import time
import hashlib

from apps.core.client import create_mysql_client
from apps.core.model import Student, PrintOrder, PrintTask
from apps.account.views import BaseHandler
from apps.file.settings import DIR_PATH, OSS_END_POINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET, LIBOFFICE_CMD

import tornado.gen
import tornado.web
from tornado.escape import json_decode

from pyPdf import PdfFileWriter, PdfFileReader
from oss.oss_api import *

# file/upload
class FileUploadHanlder(BaseHandler):

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self, *args, **kwargs):
        response = json_decode(self.request.body.decode('utf-8'))
        token = response.get('token')
        id = self.is_authorized(token)

        if id:
            cls = Student

        files = self.request.files.get('file')
        accept_file = ('pdf','doc','docx','ppt','pptx')

        for file in files:
            file_type = file['filename'].split('.')[-1]
            file_name = ''.join(file['filename'].encode("utf-8").split(" "))
            if not file_type in accept_file:
                self.return_error(msg=u'仅允许上传pdf,ppt,pptx,doc,docx')

            dir_path = DIR_PATH % (str(id), str(int(time.time())))
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
            file_path = dir_path + os.sep + file_name
            file_body = file['body']
            file_key = hashlib.md5(file_body.encode()).hexdigest()

            yield tornado.gen.Task(self.file_operation,
                                           file_type, file_path, file_body,
                                           file_key, id, dir_path, file_name)

        self.finish()

    def file_operation(self, file_type, file_path, file_body, file_key, id, dir_path, file_name, callback=None):

        with create_mysql_client() as session:
            is_exist = PrintTask.get_one(session=session, filter_by=dict(file_key=file_key))
            if is_exist:
                pages = is_exist.pages
                file_url = is_exist.file_url
            else:
                with open(file_path, 'wb') as downstream:
                    downstream.write(file_body)
                end_point = OSS_END_POINT
                accesskey_id = ACCESS_KEY_ID
                accesskey_secret = ACCESS_KEY_SECRET
                oss_entry = OssAPI(end_point, accesskey_id, accesskey_secret)
                oss_entry.put_object_from_file("student-documents", str(file_key), file_path)

                if file_type in ('pdf'):
                    pages = PdfFileReader(file(file_path,"rb")).getNumPages()
                else:
                    os.system(LIBOFFICE_CMD % (file_path, dir_path))
                    pdf_file = file_path.spilt('.')
                    pdf_file[-1] = 'pdf'
                    pages = PdfFileReader(file('.'.join(pdf_file),"rb")).getNumPages()
                file_url = os.path.abspath(file_path)
            if pages is None or file_url is None:
                self.return_error(msg=u'上传失败')
            prin_task = PrintTask.add_attrs(session, attrs=dict(student_id=id,file_name=file_name,
                                                                file_url=file_url, pages=pages))

            self.return_success(results={'pages':pages, 'task_id':prin_task.id, 'file_name':file_name})



if __name__ == '__main__':
    a = ['ddd','pdf','doc']
    print a.remove('doc')