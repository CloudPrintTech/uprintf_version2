#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import pingpp
import base64
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256

class PingppSDK(object):
    _api_key = 'sk_live_brLK841KOevPKqXHiDW9O08O'
    _app_id = 'app_WX5q5Gv5eD04iDa5'

    @classmethod
    def purchase(cls, client_ip, amount, order_no='', channel='', extra = None):
        if channel == 'alipay_pc_direct':
            try:
                charge = pingpp.Charge.create(
                    api_key = cls._api_key,
                    app = dict(id=cls._app_id),
                    order_no = order_no,
                    amount = amount,
                    client_ip = client_ip,
                    channel = channel,
                    currency='cny',
                    extra = dict(
                        success_url = extra.get('success_url'),
                    ),
                    subject = '订单支付',
                )
            except pingpp.PingppError as emsg:
                return emsg, False
            else:
                return charge, True
        elif channel == 'wx_pub_qr':
            try:
                charge = pingpp.Charge.create(
                    api_key = cls._api_key,
                    app = dict(id=cls._app_id),
                    order_no = order_no,
                    amount = amount,
                    client_ip = client_ip,
                    channel = channel,
                    currency='cny',
                    extra = dict(
                        product_id = extra.get('product_id'),
                    ),
                    subject = '订单支付',
                )
            except pingpp.PingppError as emsg:
                return emsg, False
            else:
                return charge, True
