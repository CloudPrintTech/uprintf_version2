#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from .base_config import (MYSQL_URL, REDIS_URL, OSS_END_POINT, BASE_URL_BRANCH, PORT,
                          DEBUG_MODE, ENCRYPT_PASSWORD, ENCRYPT_TOKEN)
