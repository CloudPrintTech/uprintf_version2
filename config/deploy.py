# -*- coding: utf-8 -*-

import paramiko

__author__ = 'Shuyu'

def deploy(host, username, password, debug=True):

    project_branch = 'dev' if debug else 'master'
    project_path   = '/root/uprintf/'

    try:

        print 'Start to Deploy for branch %s' % project_branch

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, 22, username, password, timeout=5)

        cmds = ['cd %s; git checkout %s; git pull origin %s;' % (project_path, project_branch, project_branch),
                'supervisorctl restart uprintf-2369;',
                'supervisorctl restart uprintf-2370;',
                'supervisorctl restart uprintf-2371;',
                'supervisorctl restart uprintf-2372;',
                'supervisorctl restart uprintf-2373;']

        for cmd in cmds:
            stdin, stdout, stderr = ssh.exec_command(cmd)

            outs = stdout.readlines()

            for out in outs:
                print out

        ssh.close()

        print 'Deploy Success for branch %s' % project_branch
    except :
        print 'Deploy Failed for branch %s' % project_branch

if __name__ == '__main__':
    deploy('58.96.191.210', 'root', 'Playstation2', True)
    # deploy('139.196.108.105', 'root', 'Playstation2', False)
