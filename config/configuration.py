#-*- coding:utf-8 -*-
__author__ = 'raytlty'

import sys
reload(sys)

class Configuration(object):
    # def __new__(cls, *args, **kwargs):
    #     if '_instance' not in vars(cls):
    #         cls._instance = super(Configuration, cls).__new__(cls, *args, **kwargs)
    #     return cls._instance

    def __init__(self):
        if len(sys.argv) > 2:
            self.port = sys.argv[1]
            self.env  = sys.argv[2]
        else:
            self.port = 2368
            self.env  = 'dev'

        self.mysql_url = 'mysql+pymysql://root:root@localhost/cloudprinter_v2?charset=utf8'
        self.redis_url = 'cerulean.me'
        self.oss_end_point = 'oss-cn-shanghai.aliyuncs.com'

        if self.env == 'dev':
            self.base_url  = 'http://api.in.uprintf.com'
        elif self.env == 'master':
            self.base_url  = 'http://api.uprintf.com'
            self.oss_end_point = 'oss-cn-shanghai-internal.aliyuncs.com'
        else:
            self.base_url  = 'http://api.in.uprintf.com'
            self.mysql_url = 'mysql+pymysql://root:root@localhost/cloudprinter_v2?charset=utf8'
    
    @property
    def get_mysql_url(self):
        return self.mysql_url
    @property
    def get_redis_url(self):
        return self.redis_url
    @property
    def get_base_url(self):
        return self.base_url
    @property
    def get_oss_end_point(self):
        return self.oss_end_point
    @property
    def get_port(self):
        return self.port
    @property
    def get_env(self):
        return self.env


if __name__ == '__main__':
    c1 = Configuration(1,2)
    c2 = Configuration(1,2)
    c1.name = 'bbpk'
    print id(c1), id(c2)
    print c1.name, c2.name


