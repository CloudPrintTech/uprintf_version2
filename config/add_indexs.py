#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from apps.core.model import Building
from apps.core.client import create_mysql_client
from sqlalchemy.sql.functions import max as MAX
import re


chinese_pattern = u"(['\u4e00''\u4e8c''\u4e09''\u56db''\u4e94''\u516d''\u4e03''\u516b''\u4e5d''\u5341''\u767e''\u5343'])+"
digest_pattern = r"([0-9])+"
english_pattern = r"([a-zA-Z])+"

mapper = {
   u'\u4e00' : 1,
   u'\u4e8c' : 2,
   u'\u4e09' : 3,
   u'\u56db' : 4,
   u'\u4e94' : 5,
   u'\u516d' : 6,
   u'\u4e03' : 7,
   u'\u516b' : 8,
   u'\u4e5d' : 9,
   u'\u5341' : 10, # 十
   u'\u767e' : 100, # 百
   u'\u5343' : 1000, # 千
    'A'       : 1,
    'B'       : 2,
    'C'       : 3,
    'D'       : 4,
    'E'       : 5,
    'F'       : 6,
    'G'       : 7,
    'H'       : 8,
    'I'       : 9,
    'J'       : 10,
    'K'       : 11,
    'L'       : 12,
    'M'       : 13,
    'N'       : 14,
    'O'       : 15,
    'P'       : 16,
    'Q'       : 17,
    'R'       : 18,
    'S'       : 19,
    'T'       : 20,
    'U'       : 21,
    'V'       : 22,
    'W'       : 23,
    'X'       : 24,
    'Y'       : 25,
    'Z'       : 26,
}

def handler():
    from collections import defaultdict
    with create_mysql_client() as session:

        max_community_id = session.query(MAX(Building.school_id)).scalar()

        for community_id in xrange(1, max_community_id + 1):

            building = session.query(Building).order_by(Building.id.asc()).filter(community_id == Building.school_id).all()
            mapper2 = defaultdict(dict)
            sign = False

            for item in building:
                change1 = change2 = change3 = 0
                idx1 = idx2 = idx3 = 0
                if re.search(chinese_pattern, item.name):
                    res = re.search(chinese_pattern, item.name).group(0)
                    idx1 = item.name.index(res) + 1
                    count = 0
                    for i in xrange(len(res)):
                        count += mapper[res[i]]
                    change1 = count

                if re.search(digest_pattern, item.name):
                    res = re.search(digest_pattern, item.name).group(0)
                    idx2 = item.name.index(res) + 1
                    change2 = int(res)

                if re.search(english_pattern, item.name):
                    res = re.search(english_pattern, item.name).group(0)
                    idx3 = item.name.index(res) + 1
                    count = 0
                    for i in xrange(len(res)):
                        count += mapper[res[i].upper()]
                    change3 = count

                sorteds = [(idx1, change1), (idx2, change2), (idx3, change3)]
                sorteds.sort()
                ans = 0
                for ele in sorteds:
                    idx, change = ele
                    if idx == 0:
                        continue
                    else :
                        ans = change
                        break

                if ans == 0:
                    sign = True
                    break
                mapper2[item] = ans

            if sign:
                for idx in xrange(1, len(building) + 1):
                    entry = building[idx - 1]
                    session.query(Building).filter(entry.id == Building.id).update(dict(indexs=idx), synchronize_session=False)
                    session.flush()
                session.commit()
                continue

            intermed = [(value, key, key.id) for key, value in mapper2.iteritems()]
            intermed.sort(key=lambda x: (x[0], x[2]))
            for i in xrange(len(intermed)):
                value, item, item_id = intermed[i]
                session.query(Building).filter(Building.id == item.id).update(dict(indexs=i+1), synchronize_session=False)
                session.flush()
            session.commit()


if __name__ == '__main__':
    handler()