#-*- coding:utf-8 -*-
__author__ = 'raytlty'

from config.configuration  import Configuration
from time import time
CONFIG = Configuration()

MYSQL_URL = CONFIG.get_mysql_url
REDIS_URL = CONFIG.get_redis_url
OSS_END_POINT = CONFIG.get_oss_end_point
BASE_URL_BRANCH = CONFIG.get_env
PORT = CONFIG.get_port

DEBUG_MODE = True

ENCRYPT_PASSWORD = '%s:%s'
ENCRYPT_TOKEN = '%s:' + str(time()) +'%s'


if __name__ == '__main__':
    print ENCRYPT_TOKEN % ('lizhihao', 'raytlty')